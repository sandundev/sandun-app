package com.sandun.tdd.promotions.order;

import com.sandun.tdd.promotions.product.Product;

public class OrderLine {

	private Product product;
	private int quantity ;
	public void setProduct(Product product) {
		this.product = product;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Product getProduct() {
		return product;
	}

	public int getQuantity() {
		return quantity;
	}

}
