package com.sandun.tdd.promotions.order;

import com.sandun.tdd.promotions.product.Product;

public class OrderBuilder {
 
	private Order order = new Order();

	public Order build() {
		return order;
	}

	public OrderBuilder addOrderLine(OrderLine orderLine) {
		order.addOrderLine(orderLine);
		return this;
	}

}
