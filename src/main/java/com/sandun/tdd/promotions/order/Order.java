package com.sandun.tdd.promotions.order;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Order {
List<OrderLine> productLine  = new ArrayList<OrderLine>();

	public BigDecimal total() {
		BigDecimal total = BigDecimal.ZERO;
		
		for (OrderLine orderLine : productLine) {
			total =	total.add(orderLine.getProduct().getCost().multiply(new BigDecimal(orderLine.getQuantity())));
		}
		return total;
	}

	public void addOrderLine(OrderLine orderLine) {
		productLine.add(orderLine);
	}

}
