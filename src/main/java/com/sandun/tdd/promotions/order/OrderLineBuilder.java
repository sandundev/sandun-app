package com.sandun.tdd.promotions.order;

import com.sandun.tdd.promotions.product.Product;

public class OrderLineBuilder {
	private OrderLine orderLine = new OrderLine();
	
	public OrderLineBuilder withProduct(Product product) {
		orderLine.setProduct(product);
		return this;
	}

	public OrderLineBuilder withQuantity(int quantity) {
		orderLine.setQuantity(quantity);
		return this;
	}

	public OrderLine build() {
		return orderLine;
	}

}
