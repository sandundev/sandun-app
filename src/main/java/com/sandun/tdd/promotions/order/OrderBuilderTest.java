package com.sandun.tdd.promotions.order;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

import com.sandun.tdd.promotions.product.ProductBuilder;
import com.sandun.tdd.promotions.product.ProductCode;

import static org.hamcrest.Matchers.equalTo;

import static org.hamcrest.Matchers.closeTo;
public class OrderBuilderTest {

	@Test
	public void orderBuildTest(){
		OrderBuilder orderBuilder = new OrderBuilder();
		
		ProductBuilder productBuilder = new ProductBuilder();
		productBuilder.withProductCode(ProductCode.GUCCI_MENS_PERFUME).withCost(new BigDecimal("100"));

		OrderLineBuilder orderLineBuilder = new OrderLineBuilder(); 
		orderLineBuilder.withProduct(productBuilder.build()).withQuantity(2);
		
		orderBuilder.addOrderLine(orderLineBuilder.build());
		
		assertThat(orderBuilder.build().total(), is(closeTo(new BigDecimal("200"), new BigDecimal("0.00"))));
	}
}
