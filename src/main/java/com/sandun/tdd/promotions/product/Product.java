package com.sandun.tdd.promotions.product;

import java.math.BigDecimal;

public class Product {

	private ProductCode productCode;
	private int quantity;
	private BigDecimal cost;

	public void setProductCode(ProductCode productCode) {
		this.productCode = productCode;
	}

	public void setQuantity(int quantity) {
		this.quantity= quantity;
		
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	public ProductCode getProductCode() {
		return productCode;
	}

	public int getQuantity() {
		return quantity;
	}

	public BigDecimal getCost() {
		return cost;
	}

}
