package com.sandun.tdd.promotions.product;

import java.math.BigDecimal;

public class ProductBuilder {
	Product product = new Product();

	public Product build() {
		return product;
	}

	public ProductBuilder withProductCode(ProductCode productCode) {
		product.setProductCode(productCode);
		return this;
	}

	public ProductBuilder withQuantity(int quantity) {
		product.setQuantity(quantity);
		return this;
	}

	public ProductBuilder withCost(BigDecimal cost) {
		product.setCost(cost);
		return this;
	}

}
